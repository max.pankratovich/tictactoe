import React from "react";
import Game from "./components/tictactoe";

function App() {
  return (
    <div>
      <h1>
        <Game />
      </h1>
    </div>
  );
}

export default App;
